/* eslint-disable */(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {filterNotifier, messaging} = require("./lib/index");
const {params} = require("./config/env");
const records = require("./config/records");

(function()
{
  window.addEventListener("message", (event) =>
  {
    if (event.data.type != "message")
      return;
    const {payload: message, messageId} = event.data;
    const sender = {
      page: new ext.Page(event.source)
    };

    const listeners = messaging.port._listeners[message.type];
    if (!listeners)
      return;

    function reply(responseMessage)
    {
      event.source.postMessage({
        type: "response",
        messageId,
        payload: responseMessage
      }, "*");
    }

    for (const listener of listeners)
    {
      const response = listener(message, sender);
      if (response && typeof response.then == "function")
      {
        response.then(
          reply,
          (reason) =>
          {
            console.error(reason);
            reply();
          }
        );
      }
      else if (typeof response != "undefined")
      {
        reply(response);
      }
    }
  });

  if (params.addSubscription)
  {
    // We don't know how long it will take for the page to fully load
    // so we'll post the message after one second
    setTimeout(() =>
    {
      let url = "https://example.com/custom.txt";

      let title = "Custom subscription";
      switch (params.addSubscription)
      {
        case "title-none":
          title = null;
          break;
        // The extension falls back to the given URL
        // when the link doesn't specify a title
        // https://hg.adblockplus.org/adblockpluschrome/file/56f54c897e3a/subscriptionLink.postload.js#l86
        case "title-url":
          title = url;
          break;
        case "invalid":
          url = url.replace("https:", "http:");
          break;
      }

      window.postMessage({
        type: "message",
        payload: {
          title, url,
          confirm: true,
          type: "subscriptions.add"
        }
      }, "*");
    }, 1000);
  }

  if (params.showPageOptions)
  {
    // We don't know how long it will take for the page to fully load
    // so we'll post the message after one second
    setTimeout(() =>
    {
      window.postMessage({
        type: "message",
        payload: {
          type: "app.open",
          what: "options",
          action: "showPageOptions",
          args: [
            {
              host: "example.com",
              allowlisted: false
            }
          ]
        }
      }, "*");
    }, 1000);
  }

  ext.devtools.onCreated.addListener((panel) =>
  {
    function getRecords(filter)
    {
      return records.filter((record) =>
      {
        const {url} = record.request;
        if (!url)
          return false;

        const pattern = url.replace(/^[\w-]+:\/+(?:www\.)?/, "");
        return filter.text.indexOf(pattern) > -1;
      });
    }

    function removeRecord(filter)
    {
      for (const record of getRecords(filter))
      {
        const idx = records.indexOf(record);
        panel.sendMessage({
          type: "remove-record",
          index: idx
        });
        records.splice(idx, 1);
      }
    }

    function updateRecord(filter)
    {
      for (const record of getRecords(filter))
      {
        record.filter = filter;
        panel.sendMessage({
          type: "update-record",
          index: records.indexOf(record),
          filter: record.filter,
          request: record.request
        });
      }
    }

    filterNotifier.filterNotifier.on("filter.added", updateRecord);
    filterNotifier.filterNotifier.on("filter.removed", removeRecord);

    for (const {filter, request} of records)
    {
      panel.sendMessage({
        type: "add-record",
        filter, request
      });
    }
  });
}());

},{"./config/env":2,"./config/records":5,"./lib/index":14}],2:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function updateFromURL(data)
{
  if (window.top.location.search)
  {
    const params = window.top.location.search.substr(1).split("&");

    for (const param of params)
    {
      const parts = param.split("=", 2);
      if (parts.length == 2 && parts[0] in data)
      {
        let value = decodeURIComponent(parts[1]);
        if (value === "false" || value === "true")
        {
          value = (value === "true");
        }
        data[parts[0]] = value;
      }
    }
  }
}

const info = {
  platform: "gecko",
  platformVersion: "34.0",
  application: "firefox",
  applicationVersion: "34.0",
  addonName: "adblockplus",
  addonVersion: "2.6.7"
};
updateFromURL(info);

const params = {
  additionalSubscriptions: "",
  addSubscription: false,
  blockedURLs: "",
  composerActive: true,
  dataCorrupted: false,
  downloadStatus: "synchronize_ok",
  filterError: null,
  filterOption: null,
  notification: null,
  domainAllowlisted: false,
  pageAllowlisted: false,
  reinitialized: false,
  showPageOptions: false
};
updateFromURL(params);

module.exports = {
  info,
  params
};

},{}],3:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const knownFilterText = [
  "! Exception rules",
  "@@||allowlisted-domain.com^$document",
  "@@|http://allowlisted-page.com/|$document",
  "@@|https://www.allowlisted-page.com/foo/bar.baz?$document",
  `@@|https://allowlisted-page.com${"/foo".repeat(20)}|$document`,
  "@@||example.com/looks_like_an_ad_but_isnt_one.html",
  "! Blocking rules",
  "||biglemon.am/bg_poster/banner.jpg",
  "/ad_banner*$domain=example.com",
  "||example.com/some-annoying-popup$popup",
  "/(example\\.com\\/some-annoying-popup\\)$/$rewrite=$1?nopopup",
  "! Hiding rules",
  "winfuture.de###header_logo_link",
  "###WerbungObenRechts10_GesamtDIV",
  "###WerbungObenRechts8_GesamtDIV",
  "###WerbungObenRechts9_GesamtDIV",
  "###WerbungUntenLinks4_GesamtDIV",
  "###WerbungUntenLinks7_GesamtDIV",
  "###WerbungUntenLinks8_GesamtDIV",
  "###WerbungUntenLinks9_GesamtDIV",
  "###Werbung_Sky",
  "###Werbung_Wide",
  "###__ligatus_placeholder__",
  "###ad-bereich1-08",
  "###ad-bereich1-superbanner",
  "###ad-bereich2-08",
  "###ad-bereich2-skyscrapper",
  "example.com##.ad_banner"
];

const filterTypes = new Set([
  "BACKGROUND",
  "CSP",
  "DOCUMENT",
  "DTD",
  "ELEMHIDE",
  "FONT",
  "GENERICBLOCK",
  "GENERICHIDE",
  "IMAGE",
  "MEDIA",
  "OBJECT",
  "OBJECT_SUBREQUEST",
  "OTHER",
  "PING",
  "POPUP",
  "SCRIPT",
  "STYLESHEET",
  "SUBDOCUMENT",
  "WEBRTC",
  "WEBSOCKET",
  "XBL",
  "XMLHTTPREQUEST"
]);

module.exports = {
  filterTypes,
  knownFilterText
};

},{}],4:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {params} = require("./env");
const {subscriptionUrls} = require("./subscriptions");

const prefsData = {
  additional_subscriptions: params.additionalSubscriptions.split(","),
  elemhide_debug: false,
  notifications_ignoredcategories: [],
  recommend_language_subscriptions: true,
  shouldShowBlockElementMenu: true,
  show_devtools_panel: true,
  show_statsinicon: true,
  subscriptions_exceptionsurl: subscriptionUrls.URL_ALLOWLIST,
  subscriptions_exceptionsurl_privacy:
    subscriptionUrls.URL_ALLOWLIST_PRIVACY,
  ui_warn_tracking: true
};

module.exports = prefsData;

},{"./env":2,"./subscriptions":6}],5:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const records = [
  // blocked request
  {
    request: {
      url: "http://adserver.example.com/ad_banner.png",
      type: "IMAGE",
      docDomain: "example.com"
    },
    filter: {
      text: "/ad_banner*$domain=example.com",
      allowlisted: false,
      userDefined: false,
      subscription: "EasyList"
    }
  },
  // allowlisted request
  {
    request: {
      url: "http://example.com/looks_like_an_ad_but_isnt_one.html",
      type: "SUBDOCUMENT",
      docDomain: "example.com"
    },
    filter: {
      text: "@@||example.com/looks_like_an_ad_but_isnt_one.html",
      allowlisted: true,
      userDefined: false,
      subscription: "EasyList"
    }
  },
  // request with long URL and no filter matches
  {
    request: {
      url: "https://this.url.has.a.long.domain/and_a_long_path_maybe_not_long_enough_so_i_keep_typing?there=are&a=couple&of=parameters&as=well&and=even&some=more",
      type: "XMLHTTPREQUEST",
      docDomain: "example.com"
    },
    filter: null
  },
  // matching element hiding filter
  {
    request: {
      type: "ELEMHIDE",
      docDomain: "example.com"
    },
    filter: {
      text: "example.com##.ad_banner",
      allowlisted: false,
      userDefined: false,
      subscription: "EasyList"
    }
  },
  // user-defined filter
  {
    request: {
      url: "http://example.com/some-annoying-popup",
      type: "POPUP",
      docDomain: "example.com"
    },
    filter: {
      text: "||example.com/some-annoying-popup$popup",
      allowlisted: false,
      userDefined: true,
      subscription: null
    }
  },
  // rewrite
  {
    request: {
      url: "http://example.com/some-annoying-popup",
      type: "OTHER",
      docDomain: "example.com",
      rewrittenUrl: "http://example.com/some-annoying-popup?nopopup"
    },
    filter: {
      text: "/(example\\.com\\/some-annoying-popup\\)$/$rewrite=$1?nopopup",
      allowlisted: false,
      userDefined: true,
      subscription: null
    }
  },
  // long filter
  {
    request: {
      type: "ELEMHIDE",
      docDomain: "example.com"
    },
    filter: {
      text: `example.com${",example.com".repeat(499)}##.ad_banner`,
      allowlisted: false,
      userDefined: false,
      subscription: "EasyList"
    }
  }
];

module.exports = records;

},{}],6:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const URL_SUBSCRIPTION_BASE = "https://easylist-downloads.adblockplus.org";
const URL_BLOCKLIST = `${URL_SUBSCRIPTION_BASE}/easylistgermany+easylist.txt`;
const URL_ALLOWLIST = `${URL_SUBSCRIPTION_BASE}/exceptionrules.txt`;
const URL_ALLOWLIST_PRIVACY =
  `${URL_SUBSCRIPTION_BASE}/exceptionrules-privacy-friendly.txt`;
const URL_DOCLINK_BASE = "https://adblockplus.org/redirect?link=";

const USER_ID = "~user~786254";

const subscriptionDetails = {
  [URL_BLOCKLIST]: {
    title: "EasyList Germany+EasyList",
    filterText: ["-ad-banner.", "-ad-big.", "-ad-bottom-", "-ad-button-"],
    installed: true
  },
  [URL_ALLOWLIST]: {
    title: "Allow non-intrusive advertising",
    installed: true
  },
  [URL_ALLOWLIST_PRIVACY]: {
    title: "Allow only nonintrusive ads that are privacy-friendly"
  },
  [`${URL_SUBSCRIPTION_BASE}/fanboy-social.txt`]: {
    title: "Fanboy's Social Blocking List",
    installed: true
  },
  [`${URL_SUBSCRIPTION_BASE}/abp-filters-anti-cv.txt`]: {
    title: "ABP Anti-Circumvention list",
    installed: true,
    disabled: false,
    recommended: "circumvention"
  },
  [`${URL_SUBSCRIPTION_BASE}/antiadblockfilters.txt`]: {
    title: "Adblock Warning Removal List",
    installed: true,
    disabled: true
  },
  [USER_ID]: {
    installed: true
  }
};

module.exports = {
  subscriptionUrls: {
    URL_DOCLINK_BASE,
    URL_ALLOWLIST,
    URL_ALLOWLIST_PRIVACY
  },
  subscriptionDetails,
  USER_ID
};

},{}],7:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function EventEmitter()
{
  this._listeners = Object.create(null);
}
EventEmitter.prototype = {
  on(name, listener)
  {
    if (name in this._listeners)
      this._listeners[name].push(listener);
    else
      this._listeners[name] = [listener];
  },
  off(name, listener)
  {
    const listeners = this._listeners[name];
    if (listeners)
    {
      const idx = listeners.indexOf(listener);
      if (idx != -1)
        listeners.splice(idx, 1);
    }
  },
  emit(name, ...args)
  {
    const listeners = this._listeners[name];
    if (listeners)
    {
      for (const listener of listeners)
        listener(...args);
    }
  }
};

module.exports = {EventEmitter};

},{}],8:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {params} = require("../config/env");
const {port} = require("./messaging");

port.on("filters.isAllowlisted", () =>
{
  return {
    hostname: !!params.domainAllowlisted,
    page: !!params.pageAllowlisted
  };
});

},{"../config/env":2,"./messaging":17}],9:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {params} = require("../config/env");

class Filter
{
  static fromText(text)
  {
    const {filterError, filterOption} = params;
    if (filterError)
      return new InvalidFilter(text, `filter_${filterError}`, filterOption);

    if (text[0] === "!")
      return new CommentFilter(text);

    return new RegExpFilter(text);
  }

  static normalize(text)
  {
    return text;
  }

  constructor(text)
  {
    this.text = text;
  }
}

class ActiveFilter extends Filter
{
  constructor(text)
  {
    super(text);
    this.disabled = false;
  }
}

class CommentFilter extends Filter {}

class InvalidFilter extends Filter
{
  constructor(text, reason, option)
  {
    super(text);
    this.reason = reason;
    this.option = option;
  }
}

class RegExpFilter extends ActiveFilter {}

module.exports = {
  ActiveFilter,
  Filter,
  InvalidFilter,
  RegExpFilter
};

},{"../config/env":2}],10:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {params} = require("../config/env");
const {port} = require("./messaging");

port.on("composer.isPageReady", () => !!params.composerActive);

},{"../config/env":2,"./messaging":17}],11:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {EventEmitter} = require("../event-emitter");

const filterNotifier = new EventEmitter();

module.exports = {filterNotifier};

},{"../event-emitter":7}],12:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const filterNotifier = require("./filter-notifier");
const {Subscription} = require("./subscription-classes");
const {subscriptionDetails, USER_ID} = require("../config/subscriptions");

const knownSubscriptions = new Map();
for (const url in subscriptionDetails)
{
  if (!subscriptionDetails[url].installed)
    continue;

  knownSubscriptions.set(
    url,
    Subscription.fromURL(url)
  );
}

const customSubscription = knownSubscriptions.get(USER_ID);

const filterStorage = {
  *subscriptions()
  {
    yield* this.knownSubscriptions.values();
  },

  get knownSubscriptions()
  {
    return knownSubscriptions;
  },

  addSubscription(subscription)
  {
    const {fromURL} = Subscription;

    if (!knownSubscriptions.has(subscription.url))
    {
      knownSubscriptions.set(subscription.url, fromURL(subscription.url));
      filterNotifier.filterNotifier.emit("subscription.added", subscription);
    }
  },

  removeSubscription(subscription)
  {
    if (knownSubscriptions.has(subscription.url))
    {
      knownSubscriptions.delete(subscription.url);
      filterNotifier.filterNotifier.emit("subscription.removed", subscription);
    }
  },

  addFilter(filter)
  {
    for (const text of customSubscription.filterText())
    {
      if (text == filter.text)
        return;
    }
    customSubscription.addFilterText(filter.text);
    filterNotifier.filterNotifier.emit("filter.added", filter);
  },

  removeFilter(filter)
  {
    customSubscription.removeFilter(filter.text);
  }
};

module.exports = {filterStorage};

},{"../config/subscriptions":6,"./filter-notifier":11,"./subscription-classes":25}],13:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const HitLogger = {
  addListener() {},
  removeListener() {}
};

module.exports = {HitLogger};

},{}],14:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const filterClasses = require("./filter-classes");
require("./filter-composer");
const filterNotifier = require("./filter-notifier");
const filterStorage = require("./filter-storage");
const hitLogger = require("./hit-logger");
const info = require("./info");
const matcher = require("./matcher");
const messaging = require("./messaging");
const notification = require("./notification");
const notificationHelper = require("./notification-helper");
const options = require("./options");
const prefs = require("./prefs");
const recommendations = require("./recommendations");
const requestBlocker = require("./request-blocker");
require("./stats");
const subscriptionClasses = require("./subscription-classes");
const subscriptionInit = require("./subscription-init");
const synchronizer = require("./synchronizer");
const utils = require("./utils");
require("./allowlisting");

const modules = {
  filterClasses,
  filterNotifier,
  filterStorage,
  hitLogger,
  info,
  matcher,
  messaging,
  notification,
  notificationHelper,
  options,
  prefs,
  recommendations,
  requestBlocker,
  subscriptionClasses,
  subscriptionInit,
  synchronizer,
  utils
};

window.require = function(module)
{
  return modules[module];
};

module.exports = modules;

},{"./allowlisting":8,"./filter-classes":9,"./filter-composer":10,"./filter-notifier":11,"./filter-storage":12,"./hit-logger":13,"./info":15,"./matcher":16,"./messaging":17,"./notification":19,"./notification-helper":18,"./options":20,"./prefs":21,"./recommendations":22,"./request-blocker":23,"./stats":24,"./subscription-classes":25,"./subscription-init":26,"./synchronizer":27,"./utils":28}],15:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {info} = require("../config/env");

module.exports = info;

},{"../config/env":2}],16:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const matcher = {
  Matcher() {},
  isSlowFilter: () => false
};

module.exports = matcher;

},{}],17:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {EventEmitter} = require("../event-emitter");

const port = new EventEmitter();

module.exports = {port};

},{"../event-emitter":7}],18:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {params} = require("../config/env");

const notifications = {
  critical: {
    type: "critical",
    title: "Critical title",
    message: "Critical <a>message</a> with <a>links</a>",
    links: ["foo", "bar"]
  },
  default: {
    title: "Default title",
    message: "Default <a>message</a> with <a>links</a>",
    links: ["foo", "bar"]
  },
  information: {
    type: "information",
    title: "Info title",
    message: "Info <a>message</a> with <a>links</a>",
    links: ["foo", "bar"]
  }
};

const notificationHelper = {
  getActiveNotification()
  {
    const name = params.notification;
    if (!name || !(name in notifications))
      return null;

    return notifications[name];
  },
  notificationClicked()
  {
  },
  shouldDisplay()
  {
    return true;
  }
};

module.exports = notificationHelper;

},{"../config/env":2}],19:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {prefs} = require("./prefs");

const notification = {
  Notification: {
    getLocalizedTexts(notif)
    {
      const texts = {};
      if ("message" in notif)
      {
        texts.message = notif.message;
      }
      if ("title" in notif)
      {
        texts.title = notif.title;
      }
      return texts;
    },
    toggleIgnoreCategory(category)
    {
      const categories = prefs.Prefs.notifications_ignoredcategories;
      const index = categories.indexOf(category);
      if (index == -1)
        categories.push(category);
      else
        categories.splice(index, 1);
      prefs.Prefs.notifications_ignoredcategories = categories;
    }
  }
};

module.exports = notification;

},{"./prefs":21}],20:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const showOptions = function()
{
  if (!/\/(?:mobile|desktop)-options\.html\b/.test(top.location.href))
    window.open("../desktop-options.html", "_blank");

  return Promise.resolve();
};

module.exports = {showOptions};

},{}],21:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {EventEmitter} = require("../event-emitter");
const prefsData = require("../config/prefs-data");

const Prefs = new EventEmitter();

for (const key of Object.keys(prefsData))
{
  Object.defineProperty(Prefs, key, {
    get()
    {
      return prefsData[key];
    },
    set(value)
    {
      prefsData[key] = value;
      Prefs.emit(key);
    }
  });
}

module.exports = {Prefs};

},{"../config/prefs-data":4,"../event-emitter":7}],22:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function requireData(filepath)
{
  const xhr = new XMLHttpRequest();
  xhr.open("GET", filepath, false);

  try
  {
    xhr.send();
    if (xhr.status !== 200)
      throw new Error("Unable to fetch file");

    return JSON.parse(xhr.responseText);
  }
  catch (ex)
  {
    return [];
  }
}

const sources = requireData("data/subscriptions.json");

function *recommendations()
{
  yield* sources;
}

module.exports = {recommendations};

},{}],23:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {filterTypes} = require("../config/filters");

module.exports = {filterTypes};

},{"../config/filters":3}],24:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {port} = require("./messaging");

port.on("stats.getBlockedPerPage", () => 123);
port.on("stats.getBlockedTotal", () => 12345);

},{"./messaging":17}],25:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const filterClasses = require("./filter-classes");
const filterNotifier = require("./filter-notifier");
const {knownFilterText} = require("../config/filters");
const {params} = require("../config/env");
const {subscriptionDetails} = require("../config/subscriptions");

function Subscription(url)
{
  this.url = url;
  this._disabled = false;
  this._lastDownload = 1234;
  this._filterText = [];
  this.homepage = "https://easylist.adblockplus.org/";
  this.downloadStatus = params.downloadStatus;

  const details = subscriptionDetails[this.url];
  if (details)
  {
    this._disabled = !!details.disabled;
    this.title = details.title || "";
    if (details.filterText)
    {
      this._filterText = details.filterText.slice();
    }
  }
  Subscription.knownSubscriptions.set(url, this);
}
Subscription.prototype =
{
  get disabled()
  {
    return this._disabled;
  },
  set disabled(value)
  {
    this._disabled = value;
    filterNotifier.filterNotifier.emit("subscription.disabled", this);
  },
  get lastDownload()
  {
    return this._lastDownload;
  },
  set lastDownload(value)
  {
    this._lastDownload = value;
    filterNotifier.filterNotifier.emit("subscription.lastDownload", this);
  },
  *filterText()
  {
    yield* this._filterText;
  }
};
Subscription.knownSubscriptions = new Map();
Subscription.fromURL = function(url)
{
  const subscription = Subscription.knownSubscriptions.get(url);
  if (subscription)
    return subscription;
  if (/^https?:\/\//.test(url))
    return new Subscription(url);
  return new SpecialSubscription(url);
};

function SpecialSubscription(url)
{
  this.url = url;
  this.disabled = false;
  this._filterText = knownFilterText.slice();
  Subscription.knownSubscriptions.set(url, this);
}
SpecialSubscription.prototype = {
  get filterCount()
  {
    return this._filterText.length;
  },
  *filterText()
  {
    yield* this._filterText;
  },
  addFilterText(filterText)
  {
    this._filterText.push(filterText);
  },
  filterTextAt(idx)
  {
    return this._filterText[idx];
  },
  removeFilter(filterText)
  {
    for (let i = 0; i < this._filterText.length; i++)
    {
      if (this._filterText[i] == filterText)
      {
        this._filterText.splice(i, 1);
        filterNotifier.filterNotifier.emit(
          "filter.removed",
          filterClasses.Filter.fromText(filterText)
        );
        return;
      }
    }
  }
};

module.exports = {
  DownloadableSubscription: Subscription,
  SpecialSubscription,
  Subscription
};

},{"../config/env":2,"../config/filters":3,"../config/subscriptions":6,"./filter-classes":9,"./filter-notifier":11}],26:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {params} = require("../config/env");

const isDataCorrupted = () => params.dataCorrupted;
const isReinitialized = () => params.reinitialized;

module.exports = {
  isDataCorrupted,
  isReinitialized
};

},{"../config/env":2}],27:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const filterNotifier = require("./filter-notifier");

const synchronizer = {
  _downloading: false,
  execute(subscription, manual)
  {
    this._downloading = true;
    filterNotifier.filterNotifier.emit(
      "subscription.downloading", subscription
    );
    setTimeout(() =>
    {
      this._downloading = false;
      subscription.lastDownload = Date.now() / 1000;
    }, 500);
  },
  isExecuting(url)
  {
    return this._downloading;
  }
};

module.exports = {synchronizer};

},{"./filter-notifier":11}],28:[function(require,module,exports){
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {subscriptionUrls} = require("../config/subscriptions");

const Utils = {
  getDocLink(link)
  {
    return `${subscriptionUrls.URL_DOCLINK_BASE}${encodeURIComponent(link)}`;
  },
  get appLocale()
  {
    return browser.i18n.getUILanguage();
  },
  get readingDirection()
  {
    return /^(?:ar|fa|he|ug|ur)\b/.test(this.appLocale) ? "rtl" : "ltr";
  }
};

module.exports = {Utils};

},{"../config/subscriptions":6}]},{},[1]);
